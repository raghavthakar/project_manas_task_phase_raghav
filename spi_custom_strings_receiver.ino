#include <SPI.h>

char slavereceived;
char message[50];
bool inflow=false;
int counter=0;

void setup()
{
  Serial.begin(115200);
  pinMode(MISO, OUTPUT);
  SPCR |= _BV(SPE);//setting slave
  SPI.attachInterrupt();
}

ISR (SPI_STC_vect)                     
{
  slavereceived = SPDR;  
  message[counter]=slavereceived;
  counter++;
  if(slavereceived=='>')
    inflow=true;
}

void loop()
{
  if(inflow==true)
  {
    inflow=false;
  	Serial.println(slavereceived);
  	counter=0;
  	delay(1000);
  }
}